The 'matscidat' folder is the top level a formal R package structure.
This package, in the 'master' branch, is the 'matscidat' package, version 0.0.1.

The package can be build to a tar.gz in linux with the following command, issued in this folder:
R CMD build matscidat

(If you change any roxygen2 headers, then don't forget to document it in an R session with the `devtools` function `document()`, beforehand)
library(devtools)
document('matscidat')

After building it to a .tar.gz file, it can be installed from an R session with:
install.packages("matscidat_0.0.1.tar.gz", repos=NULL, type='source')

A documentation pdf can be built from the R package folder structure using the bash command, issued in this folder:
R CMD Rd2pdf matscidat
