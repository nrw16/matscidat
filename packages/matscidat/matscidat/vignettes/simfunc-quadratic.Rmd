---
title: "simfunc() - Quadratic Functional Form"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{simfunc() - Quadratic Functional Form}
  %\VignetteEngine{knitr::rmarkdown}
  \usepackage[utf8]{inputenc}
---

This document demonstrates use of the `simfunc()` function to generate quadratic functional form data.

Simulated data, of the quadratic functional form, is generated with a defined noise term.

Alteration of the 3 coefficients of the quadratic functional form is explored.

```{r,fig.height=3,fig.width=9}
library("matscidat")
par(mar=c(2.25,2.25,1.25,0.5),mgp=c(1.25,0.5,0))

# Quadratic Functional Form - Standard Coefficients
sd <- simfunc(ff="quad",cf=c(0,2,6),nvals=100,xr=c(1,10),
  cnsd=c(50,0,0),xcn="var1",ycn="var2")
plot(sd,main="Quadratic")
legend("topleft","FF = var2 ~ 0 + 2*var1 + 6*var1^2 + noise(sd=50)",cex=1.25)

par(mfrow=c(2,5))
# Quadratic Functional Form - Increasing Positive First Coefficient (Intercept)
for(i in seq(0,900,length.out=10)){
  sd <- simfunc(ff="quad",cf=c(i,2,3),nvals=100,xr=c(1,10),
    cnsd=c(50,0,0),xcn="var1",ycn="var2")
  plot(sd,main=paste("Quad, Intercept = ",i,sep=""),ylim=c(-100,1500))
  legend("topleft",paste("FF = var2 ~ ",i," + 2*var1 + 3*var1^2 + noise(sd=50)",sep=""),cex=0.5)
}

# Quadratic Functional Form - Increasing Positive Second Coefficient (Slope)
for(i in seq(0,90,length.out=10)){
  sd <- simfunc(ff="quad",cf=c(0,i,3),nvals=100,xr=c(1,10),
    cnsd=c(50,0,0),xcn="var1",ycn="var2")
  plot(sd,main=paste("Quad, Slope = ",i,sep=""),ylim=c(0,1500))
  legend("topleft",paste("FF = var2 ~ 0 + ",i,"*var1 + 3*var1^2 + noise(sd=50)",sep=""),cex=0.5)
}

# Quadratic Functional Form - Decreasing Negative Second Coefficient (Slope)
for(i in seq(0,-90,length.out=10)){
  sd <- simfunc(ff="quad",cf=c(0,i,-3),nvals=100,xr=c(1,10),
    cnsd=c(50,0,0),xcn="var1",ycn="var2")
  plot(sd,main=paste("Quad, Slope = ",i,sep=""),ylim=c(-1500,0))
  legend("bottomleft",paste("FF = var2 ~ 0 + ",i,"*var1 - 3*var1^2 + noise(sd=50)",sep=""),cex=0.5)
}

# Quadratic Functional Form - Increasing Positive Third Coefficient (Quadratic)
for(i in 0:9){
  sd <- simfunc(ff="quad",cf=c(0,2,i),nvals=100,xr=c(1,10),
    cnsd=c(50,0,0),xcn="var1",ycn="var2")
  plot(sd,main=paste("Quad, Quadratic = ",i,sep=""),ylim=c(0,1000))
  legend("topleft",paste("FF = var2 ~ 0 + 2*var1 + ",i,"*var1^2 + noise(sd=50)",sep=""),cex=0.5)
}

# Quadratic Functional Form - Decreasing Negative Third Coefficient (Quadratic)
for(i in 0:-9){
  sd <- simfunc(ff="quad",cf=c(0,-2,i),nvals=100,xr=c(1,10),
    cnsd=c(50,0,0),xcn="var1",ycn="var2")
  plot(sd,main=paste("Quad, Quadratic = ",i,sep=""),ylim=c(-1000,0))
  legend("bottomleft",paste("FF = var2 ~ 0 - 2*var1 + ",i,"*var1 + noise(sd=50)",sep=""),cex=0.5)
}

# Quadratic Functional Form - Increasing Positive Second Coeff. (Slope) + Negative Third Coeff. (Quadratic)
for(i in seq(0,90,length.out=10)){
  sd <- simfunc(ff="quad",cf=c(0,i,-4),nvals=100,xr=c(1,10),
    cnsd=c(50,0,0),xcn="var1",ycn="var2")
  plot(sd,main=paste("Quad, Slope = ",i,sep=""),ylim=c(-500,750))
  legend("topleft",paste("FF = var2 ~ 0 + ",i,"*var1 - 4*var1^2 + noise(sd=50)",sep=""),cex=0.5)
}

# Quadratic Functional Form - Decreasing Negative Second Coeff. (Slope) + Positive Third Coeff. (Quadratic)
for(i in seq(0,-90,length.out=10)){
  sd <- simfunc(ff="quad",cf=c(0,i,4),nvals=100,xr=c(1,10),
    cnsd=c(50,0,0),xcn="var1",ycn="var2")
  plot(sd,main=paste("Quad, Slope = ",i,sep=""),ylim=c(-750,500))
  legend("bottomleft",paste("FF = var2 ~ 0 + ",i,"*var1 + 4*var1^2 + noise(sd=50)",sep=""),cex=0.5)
}

# Quadratic Functional Form - Negative Second Coeff. (Slope) + Increasing Positive Third Coeff. (Quadratic)
for(i in 0:9){
  sd <- simfunc(ff="quad",cf=c(0,-45,i),nvals=100,xr=c(1,10),
    cnsd=c(50,0,0),xcn="var1",ycn="var2")
  plot(sd,main=paste("Quad, Quadratic = ",i,sep=""),ylim=c(-500,500))
  legend("topleft",paste("FF = var2 ~ 0 - 45*var1 + ",i,"*var1^2 + noise(sd=50)",sep=""),cex=0.5)
}

# Quadratic Functional Form - Positive Second Coeff. (Slope) +  Decreasing Negative Third Coeff. (Quadratic)
for(i in 0:-9){
  sd <- simfunc(ff="quad",cf=c(0,45,i),nvals=100,xr=c(1,10),
    cnsd=c(50,0,0),xcn="var1",ycn="var2")
  plot(sd,main=paste("Quad, Quadratic = ",i,sep=""),ylim=c(-500,500))
  legend("bottomleft",paste("FF = var2 ~ 0 + 45*var1 + ",i,"*var1 + noise(sd=50)",sep=""),cex=0.5)
}


```
