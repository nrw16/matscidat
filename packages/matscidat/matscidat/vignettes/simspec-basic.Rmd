---
title: "simspec() - Basic Spectral Definition"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{simspec() - Basic Spectral Definition}
  %\VignetteEngine{knitr::rmarkdown}
  \usepackage[utf8]{inputenc}
---

This document demonstrates use of the `simspec()` function to generate basic spectral data.

Simulated data, in the form of an artificial spectra, is generated with defined peak heights, locations and widths, and a defined noise term.

Alteration of the parameters of the function is explored.

```{r,fig.height=3,fig.width=9}
library("matscidat")
par(mar=c(2.25,2.25,1.25,0.5),mgp=c(1.25,0.5,0))

# Basic simspec() Demonstration - some Standard Parameters
ss <- simspec(nvals=1000,xr=c(250,500),bln=0.015,ph=c(1,1,0.5),pl=c(300,350,400),pw=c(20,50,199),xcn="Wavelength",ycn="Amplitude")
plot(ss,type='l',ylim=c(0,1.5))
legend("topleft","Synthetic Spectra From simspec() Function",cex=1.25)
```

```{r,fig.height=3,fig.width=9}
par(mfrow=c(2,5))
# Baseline Noise Parameter - Increasing Standard Deviation of Baseline Noise
for(i in seq(0,0.45,length.out=10)){
  ss <- simspec(nvals=1000,xr=c(250,500),bln=i,ph=c(1,1,0.5),pl=c(300,350,400),pw=c(20,50,199),xcn="Wavelength",ycn="Amplitude")
  plot(ss,type='l',main=paste("BL Noise = ",i,sep=""),ylim=c(-0.5,1.5))
  legend("topleft",paste("BL Noise = ",i,sep=""),cex=0.75)
}
```

```{r,fig.height=3,fig.width=9}
# Peak Height Parameter - Varying the Peak Height Parameter
for(i in seq(0.1,1,length.out=10)){
  ss <- simspec(nvals=2000,xr=c(0,500),bln=0,ph=i,pl=250,pw=100,xcn="Wavelength",ycn="Amplitude")
  plot(ss,type='l',main=paste("Peak Height = ",i,sep=""),ylim=c(-0.5,1.5))
  lines(c(0,25),rep(i,2),col='red')
  legend("topleft",paste("Peak Height = ",i,sep=""),cex=0.75)
}
```

```{r,fig.height=3,fig.width=9}
# Peak Location Parameter - Varying the Peak Location Parameter
for(i in seq(36*2,360+36,length.out=10)){
  ss <- simspec(nvals=2000,xr=c(0,500),bln=0,ph=1,pl=i,pw=100,xcn="Wavelength",ycn="Amplitude")
  plot(ss,type='l',main=paste("Peak Location = ",i,sep=""),ylim=c(-0.5,1.5))
  lines(rep(i,2),c(-0.1,0.1),col='red')
  legend("topleft",paste("Peak Location = ",i,sep=""),cex=0.75)
}
```

```{r,fig.height=3,fig.width=9}
# Peak Width Parameter - Varying the Peak Width Parameter
for(i in seq(45,450,length.out=10)){
  ss <- simspec(nvals=2000,xr=c(0,500),bln=0,ph=1,pl=250,pw=i,xcn="Wavelength",ycn="Amplitude")
  plot(ss,type='l',main=paste("Peak Width = ",i,sep=""),ylim=c(-0.5,1.5))
  lines(rep(250-(0.5*i),2),c(-0.1,0.1),col='red')
  lines(rep(250+(0.5*i),2),c(-0.1,0.1),col='red')
  legend("topleft",paste("Peak Width = ",i,sep=""),cex=0.75)
}
```
