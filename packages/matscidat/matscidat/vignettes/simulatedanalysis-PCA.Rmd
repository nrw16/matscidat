---
title: "Simulated Analysis - PCA"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Simulated Analysis - PCA}
  %\VignetteEngine{knitr::rmarkdown}
  \usepackage[utf8]{inputenc}
---

This document demonstrates use of the functions in the `matscidat` package to generate simulated data for the purpose of demonstrating the PCA statistical analytics procedure as applicable to materials science datasets.


```{r,fig.height=3,fig.width=9}
library("matscidat")
par(mar=c(2.25,2.25,1.25,0.5),mgp=c(1.25,0.5,0))

# Define simulated data vectors of a known functional forms to drive chemical changes

# Utilize the functional form data to create a series of simulated spectra

# Utilize PCA methods to extract the spectral feature variation modes

# Compare PCA results to input simulated functional form data

# simspec() Demonstration - Utilizing a List Object to Define Peaks
# Define an empty peak list object
plist <- list(c(vector(),vector(),vector()))
# Define the peak heights, locations, and widths
plist[[1]] <- c(1.0,1.0,0.5,1.0,0.5,1.0,0.2,0.9,0.4,0.7) # Peak Heights
plist[[2]] <- c(100,120,180,225,290,450,600,725,800,900) # Peak Locations
plist[[3]] <- c(045,080,100,300,100,150,030,200,100,080) # Peak Widths
# Utilize the peak list object to generate a simulated spectra with simspec()
ss <- simspec(nvals=4000,xr=c(0,1000),bln=0.015,ph=plist[[1]],pl=plist[[2]],pw=plist[[3]])
plot(ss[[1]],ss[[2]],type='l',xlab="Wavelength",ylab="Amplitude",ylim=c(0,1.5))
legend("topleft","Simulated Spectra From simspec() Function",cex=1.25)

```
