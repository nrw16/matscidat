# This is a Placeholder README for the repomatic repository structure

## This specific readme.md is for the root repository folder

It can be changed by modifying the file 'readme-root.md' in the 'inst/extdata/' folder in the package file structure, and then running the 'writemdrdas()' function with the path of the repository directory (where the DESCRIPTION file is) as the argument.

Running this function will update the 'root_mdlv.rda' file of the 'repomatic' package, which contains the lines of text in this markdown as a vector of character elements ('root_mdlv'), to match the contents of the 'inst/extdata/readme-root.md' file.
